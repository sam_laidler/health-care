@echo off
set /p appName="Enter app name (default=health-care): " || set "appName=health-care"
cp ..\..\..\target\myapp.jar .
docker build --tag=%appName% .
docker run -it %appName%