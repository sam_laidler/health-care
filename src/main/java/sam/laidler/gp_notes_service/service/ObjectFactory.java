
package sam.laidler.gp_notes_service.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the sam.laidler.gp_notes_service.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetNotesResponse_QNAME = new QName("http://service.gp_notes_service.laidler.sam/", "getNotesResponse");
    private final static QName _GetNotes_QNAME = new QName("http://service.gp_notes_service.laidler.sam/", "getNotes");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: sam.laidler.gp_notes_service.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetNotesResponse }
     * 
     */
    public GetNotesResponse createGetNotesResponse() {
        return new GetNotesResponse();
    }

    /**
     * Create an instance of {@link GetNotes }
     * 
     */
    public GetNotes createGetNotes() {
        return new GetNotes();
    }

    /**
     * Create an instance of {@link GpNote }
     * 
     */
    public GpNote createGpNote() {
        return new GpNote();
    }

    /**
     * Create an instance of {@link MedicalHistory }
     * 
     */
    public MedicalHistory createMedicalHistory() {
        return new MedicalHistory();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNotesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.gp_notes_service.laidler.sam/", name = "getNotesResponse")
    public JAXBElement<GetNotesResponse> createGetNotesResponse(GetNotesResponse value) {
        return new JAXBElement<GetNotesResponse>(_GetNotesResponse_QNAME, GetNotesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetNotes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.gp_notes_service.laidler.sam/", name = "getNotes")
    public JAXBElement<GetNotes> createGetNotes(GetNotes value) {
        return new JAXBElement<GetNotes>(_GetNotes_QNAME, GetNotes.class, null, value);
    }

}
