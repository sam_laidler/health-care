package sam.laidler.health_care.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.xml.ws.WebServiceRef;

import sam.laidler.gp_notes_service.service.MedicalHistory;
import sam.laidler.gp_notes_service.service.MedicalHistoryService;
import sam.laidler.gp_notes_service.service.MedicalHistoryServiceService;
import sam.laidler.health_care.domain.MedHistoryDTO;
import sam.laidler.health_care.domain.Patient;

@Stateless
public class PatientManagement implements PatientManagementInterface {
	// Very crude stub implementation will do for now
	List<Patient> patients = new ArrayList<Patient>();
	
	@WebServiceRef
	MedicalHistoryServiceService medHistoryServiceRef;

	public void registerPatient(Patient patient) throws PatientManagementException {
		if (patients.contains(patient)) {
			throw new PatientManagementException(patient + " already registered");
		}

		MedicalHistoryService medHistoryService = medHistoryServiceRef.getMedicalHistoryServicePort();
		if (medHistoryService != null) {
			MedicalHistory medHistory = medHistoryService.getNotes(patient.getNhsNumber());
			if (medHistory != null) {
				patient.setGpHistory(new MedHistoryDTO(medHistory));
			}
		}
		
		patients.add(patient);
		patients.sort((p1, p2) -> p1.getFamilyName().compareTo(p2.getFamilyName()));
	}

	public boolean removePatient(Patient patient) {
		return patients.remove(patient);
	}

	public List<Patient> getPatients() {
		return patients;
	}

	public List<Patient> getPatientsByFamilyName(String familyName) {
		ArrayList<Patient> matchingPatients = new ArrayList<>();
		matchingPatients.addAll(patients);
		matchingPatients.removeIf(p -> !p.getFamilyName().matches(familyName));
		return matchingPatients;
	}
	
	public Patient getPatientByNHSNumber(int nhsNumber) throws PatientManagementException {
		int index = 0;
		
		while (index < patients.size()) {
			Patient patient = patients.get(index);
			if (patient.getNhsNumber() == nhsNumber) {
				return patient;
			}
		}
		
		throw new PatientManagementException("Patient number not found");
	}
}
