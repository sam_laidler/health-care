package sam.laidler.health_care.service;

@SuppressWarnings("serial")
public class PatientManagementException extends Exception {
	public PatientManagementException(String message) {
		super(message);
	}
}
