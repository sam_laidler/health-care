package sam.laidler.health_care.service;

import java.util.List;

import javax.ejb.Remote;

import sam.laidler.health_care.domain.Patient;

@Remote
public interface PatientManagementInterface {
	void registerPatient(Patient patient) throws PatientManagementException;
	boolean removePatient(Patient patient);
	public List<Patient> getPatients();
	List<Patient> getPatientsByFamilyName(String familyName);
	public Patient getPatientByNHSNumber(int nhsNumber) throws PatientManagementException;
}
