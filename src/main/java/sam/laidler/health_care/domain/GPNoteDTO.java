package sam.laidler.health_care.domain;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;

import sam.laidler.gp_notes_service.service.GpNote;

public class GPNoteDTO extends GpNote implements Serializable {
	private XMLGregorianCalendar date;
	private String notes;
	
	public GPNoteDTO(GpNote note) {
		this.date = note.getDate();
		this.notes = note.getNotes();
	}

	@Override
	public String toString() {
		return "GPNoteDTO [date=" + date + ", notes=" + notes + "]";
	}
}
