package sam.laidler.health_care.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import sam.laidler.gp_notes_service.service.GpNote;
import sam.laidler.gp_notes_service.service.MedicalHistory;

public class MedHistoryDTO extends MedicalHistory implements Serializable {
	private String gpName;
	private List<GPNoteDTO> notes;
	
	public MedHistoryDTO(MedicalHistory mh) {
		this.gpName = mh.getGpName();
		this.notes = new ArrayList<>();
		for (GpNote note : mh.getNotes()) {
			this.notes.add(new GPNoteDTO(note));
		}
	}

	@Override
	public String toString() {
		return "MedHistoryDTO [gpName=" + gpName + ", notes=" + notes + "]";
	}
}
