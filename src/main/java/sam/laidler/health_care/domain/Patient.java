package sam.laidler.health_care.domain;

import java.io.Serializable;

import sam.laidler.gp_notes_service.service.MedicalHistory;

@SuppressWarnings("serial")
public class Patient implements Serializable {
	private String firstName;
	private String familyName;
	private int nhsNumber;
	private MedHistoryDTO gpHistory;

	public Patient(String firstName, String familyName, int nhsNumber) {
		super();
		this.firstName = firstName;
		this.familyName = familyName;
		this.nhsNumber = nhsNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public int getNhsNumber() {
		return nhsNumber;
	}

	public void setNhsNumber(int nhsNumber) {
		this.nhsNumber = nhsNumber;
	}

	public MedicalHistory getGpHistory() {
		return gpHistory;
	}

	public void setGpHistory(MedHistoryDTO gpHistory2) {
		this.gpHistory = gpHistory2;
	}

	@Override
	public String toString() {
		return "Patient [firstName=" + firstName + ", familyName=" + familyName + ", nhsNumber=" + nhsNumber
				+ ", gpHistory=" + gpHistory + "]";
	}

	@Override
	public int hashCode() {
		return nhsNumber;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Patient) {
			Patient p = (Patient) obj;
			return nhsNumber == p.getNhsNumber();
		}
		return false;
	}
}
