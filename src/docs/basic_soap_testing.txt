May 05, 2019 3:41:12 PM org.wildfly.naming.client.Version <clinit>
INFO: WildFly Naming version 1.0.1.Final
May 05, 2019 3:41:12 PM org.xnio.Xnio <clinit>
INFO: XNIO version 3.5.1.Final
May 05, 2019 3:41:12 PM org.xnio.nio.NioXnio <clinit>
INFO: XNIO NIO Implementation Version 3.5.1.Final
May 05, 2019 3:41:12 PM org.jboss.remoting3.EndpointImpl <clinit>
INFO: JBoss Remoting version 5.0.0.Final
May 05, 2019 3:41:12 PM org.wildfly.security.Version <clinit>
INFO: ELY00001: WildFly Elytron version 1.1.1.Final
May 05, 2019 3:41:13 PM org.jboss.ejb.client.EJBClient <clinit>
INFO: JBoss EJB Client version 4.0.0.Final

Choose option
1 = register new patient
2 = remove patient
3 = print all patients
4 = print patients by family name
5 = exit
6 = print these options

1
Enter first name
sam
Enter family name
laidler
Enter NHS number
1

Choose option
1 = register new patient
2 = remove patient
3 = print all patients
4 = print patients by family name
5 = exit
6 = print these options

1
Enter first name
dan
Enter family name
jones
Enter NHS number
2

Choose option
1 = register new patient
2 = remove patient
3 = print all patients
4 = print patients by family name
5 = exit
6 = print these options

1
Enter first name
mike
Enter family name
smith
Enter NHS number
3

Choose option
1 = register new patient
2 = remove patient
3 = print all patients
4 = print patients by family name
5 = exit
6 = print these options

3
Printing all patients...
Patient [firstName=dan, familyName=jones, nhsNumber=2, gpHistory=MedHistoryDTO [gpName=Birmingham General Admissions, notes=[GPNoteDTO [date=2019-05-05T14:41:43.948Z, notes=Slight cough], GPNoteDTO [date=2019-05-05T14:41:43.948Z, notes=Loss of hearing]]]]
Patient [firstName=sam, familyName=laidler, nhsNumber=1, gpHistory=MedHistoryDTO [gpName=London St Marys, notes=[GPNoteDTO [date=2019-05-05T14:41:43.948Z, notes=Back pain], GPNoteDTO [date=2019-05-05T14:41:43.948Z, notes=Dry eyes]]]]
Patient [firstName=mike, familyName=smith, nhsNumber=3, gpHistory=MedHistoryDTO [gpName=The Manchester Clinic, notes=[GPNoteDTO [date=2019-05-05T14:41:43.948Z, notes=Back pain], GPNoteDTO [date=2019-05-05T14:41:43.948Z, notes=Severe headache], GPNoteDTO [date=2019-05-05T14:41:43.948Z, notes=Dry eyes], GPNoteDTO [date=2019-05-05T14:41:43.948Z, notes=Loss of hearing], GPNoteDTO [date=2019-05-05T14:41:43.948Z, notes=Broken jaw]]]]

Choose option
1 = register new patient
2 = remove patient
3 = print all patients
4 = print patients by family name
5 = exit
6 = print these options

